<?php

/* Affiche un message en en-tête des pages de l'espace privé */
function cmw_alertes_auteur($flux){
    if ($flux['args']['exec']=='accueil') {
        // On ajoute une alerte
        $flux['data'][] = '<div class="box notice">
        <!--
        <div class="box__header titrem clearfix">
        <h3>Besoin d\'aide ?</h3>
        </div>
        -->
        <div class="box__body clearfix" style="text-align: left;">
        Si vous avez besoin d\'aide sur l\'utilisation du site, par exemple pour modifier votre page personnelle, vous pouvez consulter la rubrique <a href="../spip.php?rubrique86">Aide site Web</a>.
        </div>
        </div>';
    }
    
    // On retourne le flux éventuellement modifié
    return $flux;
}

/* Passe le statut d'un nouvel article à "publié" automatiquement si l'auteur est admin */
// function cmw_pre_insertion($flux) {
//     if (isset($flux['args']['table']) and
//     $flux['args']['table']=='spip_articles' and
//     $GLOBALS['auteur_session']['statut'] == '0minirezo') {
//         $flux['data']['statut'] = 'publie';
//     }
//     if (isset($flux['args']['table']) and
//     $flux['args']['table']=='spip_publis' and
//     $GLOBALS['auteur_session']['statut'] == '0minirezo') {
//         $flux['data']['statut'] = 'publie';
//     }
//     return $flux;
// }

/* Message sur écran de connexion */
function cmw_recuperer_fond($flux) {
    if ($flux['args']['fond']=='formulaires/login') {
        $alert = '<div class="box notice" style="position: absolute; top: 20%; left: 50%; transform: translate(-50%, -50%);">
        <div class="box__body clearfix" style="text-align: left;">
        Besoin d\'aide ? Consultez la page <a href="spip.php?article14" style="color: #ffda84;">Comment se connecter au site</a>.
        </div>
        </div>';
        $flux['data']['texte'] = $alert.$flux['data']['texte'];
    }
    return($flux);
}


?>