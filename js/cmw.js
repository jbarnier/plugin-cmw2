$(document).ready(function( $ ){

    /* Dropdown on hover */

    // $('.navbar [data-toggle="dropdown"]').bootstrapDropdownHover({
    //     'setClickBehavior': 'disable',
    //     'hideTimeout': 500
    // });

    // Add animation
    // $('.dropdown').on('show.bs.dropdown', function () {
    //   $(this).find('.dropdown-menu').first().stop().slideDown(200);
    //   $(this).find('.arrow-up').first().stop().show();
    // })
    // $('.dropdown').on('hide.bs.dropdown', function () {
    //     $(this).find('.dropdown-menu').first().stop().slideUp(200);
    //     $(this).find('.arrow-up').first().stop().hide();
    // })


    /* Leaflet */

    var CartoDB_Positron_msh = L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png', {
	    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>',
	    subdomains: 'abcd',
	    maxZoom: 19
    });

    var CartoDB_Positron_ens = L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png', {
	    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>',
	    subdomains: 'abcd',
	    maxZoom: 19
    });

    var CartoDB_Positron_bron = L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png', {
	    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>',
	    subdomains: 'abcd',
	    maxZoom: 19
    });
    
    var CartoDB_Positron_sainte = L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png', {
	    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>',
	    subdomains: 'abcd',
	    maxZoom: 19
    });

    var CartoDB_Positron_infos_pratiques = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	    maxZoom: 19,
	    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }); 

    L.Icon.Default.prototype.options['imagePath'] = 'plugins/cmw2/img/';
    L.Icon.Default.prototype.options['iconUrl'] = 'marker-icon.png';
    L.Icon.Default.prototype.options['iconRetinaUrl'] = 'marker-icon.png';
    L.Icon.Default.prototype.options['shadowUrl'] = 'marker-shadow.png';

    var map_msh = L.map('map-msh', {scrollWheelZoom: false}).setView([45.74693, 4.83518], 13);
    var marker_msh = L.marker([45.74693, 4.83518]).addTo(map_msh);
    marker_msh.bindPopup("Site Lyon Berthelot");
    CartoDB_Positron_msh.addTo(map_msh);

    var map_ens = L.map('map-ens', {scrollWheelZoom: false}).setView([45.73363, 4.83379], 12);
    var marker_ens = L.marker([45.73363, 4.83379]).addTo(map_ens);
    marker_ens.bindPopup("Site Lyon ENS");
    CartoDB_Positron_ens.addTo(map_ens);

    var map_bron = L.map('map-bron', {scrollWheelZoom: false}).setView([45.72128, 4.91775], 11);
    var marker_bron = L.marker([45.72128, 4.91775]).addTo(map_bron);
    marker_bron.bindPopup("Site Bron<br>Campus Porte des Alpes");
    CartoDB_Positron_bron.addTo(map_bron);

    var map_sainte = L.map('map-sainte', {scrollWheelZoom: false}).setView([45.42608, 4.3934], 13);
    var marker_sainte = L.marker([45.42608, 4.3934]).addTo(map_sainte);
    marker_sainte.bindPopup("Site Saint-Étienne")
    CartoDB_Positron_sainte.addTo(map_sainte);

    if ($('#map_infos_pratiques').length) {
        L.Icon.Default.prototype.options['imagePath'] = 'plugins/cmw2/img/';
        L.Icon.Default.prototype.options['iconUrl'] = 'marker-icon-red.png';
        L.Icon.Default.prototype.options['iconRetinaUrl'] = 'marker-icon-red.png';
        L.Icon.Default.prototype.options['shadowUrl'] = 'marker-shadow.png';

        var map_infos_pratiques = L.map('map_infos_pratiques', {scrollWheelZoom: true}).setView([45.6094, 4.6338],   10);
        var marker_sainte_infos = L.marker([45.42608, 4.3934]).addTo(map_infos_pratiques);
        marker_sainte_infos.bindPopup("Site Saint-Étienne");
        var marker_bron_infos = L.marker([45.72128, 4.91775]).addTo(map_infos_pratiques);
        marker_bron_infos.bindPopup("Site Bron<br>Campus Porte des Alpes");
        var marker_ens_infos = L.marker([45.73363, 4.83379]).addTo(map_infos_pratiques);
        marker_ens_infos.bindPopup("Site Lyon ENS");
        var marker_msh_infos = L.marker([45.74693, 4.83518]).addTo(map_infos_pratiques);
        marker_msh_infos.bindPopup("Site Lyon Berthelot");
        CartoDB_Positron_infos_pratiques.addTo(map_infos_pratiques);
    }

    /* Filtrage listes */

    var options = {
        valueNames: [ 'searchtext' ]
      };
    $('.listjs').each(function(index, elem) {
        var listObj = new List(elem, options);
        $('#search-list').on('keyup', function() {
            var searchString = $(this).val();
            listObj.search(searchString);
        });
        listObj.on('updated', function(list) {
            var searchString = $('#search-list').val();
            var nb_results = list.matchingItems.length;
            var tabid = $(list.listContainer).attr("aria-labelledby");
            if (searchString != "" && nb_results > 0) {
                $('#' + tabid).find('span').text("(" + nb_results + ")");
            } else {
                $('#' + tabid).find('span').text("");
            }

            if (nb_results == 0) {
                $(list.listContainer).find('.list-nothing').show();
            } else {
                $(list.listContainer).find('.list-nothing').hide();
            }
        })
    })


    /* Tooltips */
    $('[data-toggle="tooltip"]').tooltip()

    /* Back to top */
    $("#toTop").click(function () {
        //1 second of animation time
        //html works for FFX but not Chrome
        //body works for Chrome but not FFX
        //This strange selector seems to work universally
        $("html, body").animate({scrollTop: 0}, 1000);
     });
     if($(window).width() <= 1024) {
        $(window).scroll(function() {
            if ($(this).scrollTop() > 500) {
                $('#toTop').addClass('visible');
            } else {
                $('#toTop').removeClass('visible');
            }
        });
    }
})