<?php

setlocale(LC_TIME, "fr_FR");

function get_hal_api_url($hal, $prenom, $nom, $hide_typdoc, $nb_recentes=0, $complete=0) {

    $code_hal_cmw = "136794";

    $fields = array("docType_s", "conferenceTitle_s", "scientificEditor_s", "bookTitle_s", "journalTitle_s", "title_s", "description_s", "volume_s", "issue_s", "conferenceStartDate_s", "publicationDate_s", "producedDate_s", "defenseDate_s", "page_s", "subTitle_s", "halId_s", "serie_s", "publisher_s", "authFullName_s", "journalPublisher_s", "city_s", "files_s", "linkExtUrl_s", "authQuality_s", "structHasAuthId_fs");
    $fields = "&fl=".implode(",", $fields);
    
    if (preg_match('/[a-z]/i', $hal)) {
        // Si idHal renseigné
        $query = "q=authIdHal_s:".$hal;
    } else {
        $prenom = trim(urlencode($prenom));
        $nom = trim(urlencode($nom));
        $query = "q=authFullName_t:\"".$prenom."%20".$nom."\"%20AND%20structId_i:".$code_hal_cmw;
    }

    if ($complete == 1) {
        $sort = "&sort=docType_s+asc,publicationDate_s+desc";
    } else {
        $sort = "&sort=publicationDate_s+desc";
    }

    if ($complete == 1) {
        $rows = "&rows=500";
    } else {
        $rows = "&rows=".($nb_recentes > 0 ? $nb_recentes : 10);
    }

    
    $filter = "";
    if (sizeof($hide_typdoc) > 0) {
        $hide_typdoc = array_map("strtoupper", $hide_typdoc);
        $filter = "&fq=-docType_s:(".implode("%20OR%20", $hide_typdoc).")";
    }

    $url = "https://api.archives-ouvertes.fr/search/?".$query.$fields.$sort.$filter.$rows."&wt=json";

    return $url;
}


function import_hal_api_recentes($data) {
    $out = import_hal($data, $recentes=true);
    return($out);
}


function import_hal_api_complete($data) {
    $out = import_hal($data, $recentes=false);
    return($out);
}


function import_hal($data, $recentes) {

    $typdoc = $data['docType_s'];

    switch($typdoc) {
    case "COUV":
        $nom_typdoc = "Chapitre d'ouvrage";
        $result = hal_api_import_couv($data);
        break;
    case "COMM":
        $nom_typdoc = "Communication";
        $result = hal_api_import_comm($data);
        break;
    case "ART":
        $nom_typdoc = "Article";
        $result = hal_api_import_art($data);
        break;
    case "ISSUE":
        $nom_typdoc = "Numéro spécial de revue";
        $result = hal_api_import_art($data);
        break;
    case "PROCEEDINGS":
        $nom_typdoc = "Recueil de communications";
        $result = hal_api_import_ouv($data);
        break;   
    case "OUV":
        $nom_typdoc = "Ouvrage";
        $result = hal_api_import_ouv($data);
        break;
    case "DOUV":
        $nom_typdoc = "Direction d'ouvrage";
        $result = hal_api_import_douv($data);
        break;
    case "REPORT":
        $nom_typdoc = "Rapport";
        $result = hal_api_import_report($data);
        break;
    case "OTHER":
        $nom_typdoc = "Autres publications";
        $result = hal_api_import_other($data);
        break;
    case "UNDEFINED":
        $nom_typdoc = "Document de travail";
        $result = hal_api_import_preprint($data);
        break;
    case "THESE":
        $nom_typdoc = "Thèse";
        $result = hal_api_import_these($data);
        break;
    case "HDR":
        $nom_typdoc = "HDR";
        $result = hal_api_import_hdr($data);
        break;
    case "SOFTWARE":
        $nom_typdoc = "Logiciel";
        $result = hal_api_import_other($data);
        break;
    case "VIDEO":
        $nom_typdoc = "Vidéo";
        $result = hal_api_import_other($data);
        break;
    default:
        $nom_typdoc = "";
        $result = hal_api_import_other($data);
        $result="";
    }
    $out = $result;
    if ($recentes) {
        $out .= '<div class="hal-icons">';
        if ($nom_typdoc!="" && $recentes) $out .= '<span class="label-typdoc label-'.$typdoc.'">'.$nom_typdoc.'</span>';
        $out .= hal_api_extract_liens($data, $recentes);
        $out .= '</div>';
    }
    else {
        $out .= '<div class="hal-icons">';
        $out .= hal_api_extract_liens($data, $recentes);
        $out .= '</div>';
    }
        
    return($out);
}


// Communication
function hal_api_import_comm($data) {
    $out = "";
    $out .= hal_api_extract_auteurs($data);
    $out .= hal_api_extract_title($data, $type="second");
    // Titre conférence
    $q = $data['conferenceTitle_s'];
    if (!empty($q)) {
        $out .= ", <em>".$q."</em>";
    }
    // Ville conférence
    $q = $data['city_s'];
    if (!empty($q)) {
        $out .= ", ". $q;
    }
    $out .= hal_api_extract_datestart($data);
    return($out);
}

// Chapitre d'ouvrage
function hal_api_import_couv($data) {
    $out = "";
    $out .= hal_api_extract_auteurs($data);
    $out .= hal_api_extract_title($data, $type="second");
    $out .= " <em>in</em> ";
    // Auteurs ouvrage
    $q = $data['scientificEditor_s'];
    if (is_array($q)) {
        if (count($q)>0) $out .= $q[0] . " (dir.), ";
    }
    // Titre ouvrage
    $q = $data['bookTitle_s'];
    $out .= "<em>".$q."</em>";
    $out .= hal_api_extract_volumaison($data);
    $out .= hal_api_extract_editeur($data);
    $out .= hal_api_extract_collection($data);
    $out .= hal_api_extract_datepub($data, "%Y");
    $out .= hal_api_extract_pagination($data, "pos");
    return($out);
}

// Autres publications
function hal_api_import_other($data) {
    $out = "";
    $out .= hal_api_extract_auteurs($data);
    // Titre ouvrage
    $q = $data['bookTitle_s'];
    if (!empty($q)) {
        $type = "second";
        $tmptitle = ", <em>".$q."</em>";
    }
    else {
        $type = "main";
        $tmptitle = "";
    }
    $out .= hal_api_extract_title($data, $type=$type);
    $out .= $tmptitle;
    $out .= hal_api_extract_description($data);
    $out .= hal_api_extract_datepub($data);
    return($out);
}

// Pre-prints
function hal_api_import_preprint($data) {
    $out = "";
    $out .= hal_api_extract_auteurs($data);
    // Titre ouvrage
    $q = $data['bookTitle_s'];
    if (!empty($q)) {
        $type = "second";
        $tmptitle = ", <em>".$q."</em>";
    }
    else {
        $type = "main";
        $tmptitle = "";
    }
    $out .= hal_api_extract_title($data, $type=$type);
    $out .= $tmptitle;
    $out .= hal_api_extract_description($data);
    $out .= hal_api_extract_dateprod($data);
    return($out);
}

// Article de revue
function hal_api_import_art($data) {
    $out = "";
    $out .= hal_api_extract_auteurs($data);
    $out .= hal_api_extract_title($data, $type="second");
    // Titre revue
    $q = $data['journalTitle_s'];
    if (!empty($q)) {
        $out .= ", <em>".$q."</em>";
    }
    $out .= hal_api_extract_volumaison($data);
    $out .= hal_api_extract_editeur($data);
    $out .= hal_api_extract_datepub($data);
    $out .= hal_api_extract_pagination($data, "pos");
    return($out);
}

// Rapport
function hal_api_import_report($data) {
    $out = "";
    $out .= hal_api_extract_auteurs($data);
    $out .= hal_api_extract_title($data, $type="main");
    $out .= hal_api_extract_editeur($data);
    $out .= hal_api_extract_collection($data);
    $out .= hal_api_extract_datepub($data, "%Y");
    $out .= hal_api_extract_pagination($data, "nb");
    return($out);
}

// Thèse
function hal_api_import_these($data) {
    $out = "";
    $out .= hal_api_extract_auteurs($data);
    $out .= hal_api_extract_title($data, $type="main");
    $out .= hal_api_extract_datesoutenance($data);
    $out .= hal_api_extract_datepub($data, "%Y");
    $out .= hal_api_extract_pagination($data, "nb");
    return($out);
}

// Thèse
function hal_api_import_hdr($data) {
    $out = "";
    $out .= hal_api_extract_auteurs($data);
    $out .= hal_api_extract_title($data, $type="main");
    $out .= hal_api_extract_datesoutenancehdr($data);
    $out .= hal_api_extract_datepub($data, "%Y");
    $out .= hal_api_extract_pagination($data, "nb");
    return($out);
}

// Ouvrage
function hal_api_import_ouv($data) {
    $out = "";
    $out .= hal_api_extract_auteurs($data);
    $out .= hal_api_extract_title($data, $type="main");
    $out .= hal_api_extract_editeur($data);
    $out .= hal_api_extract_collection($data);
    $out .= hal_api_extract_datepub($data, "%Y");
    $out .= hal_api_extract_pagination($data, "nb");
    return($out);
}

// Direction d'ouvrage
function hal_api_import_douv($data) {
    $out = "";
    $out .= hal_api_extract_auteurs($data);
    $out .= hal_api_extract_title($data, $type="main");
    $out .= hal_api_extract_editeur($data);
    $out .= hal_api_extract_collection($data);
    $out .= hal_api_extract_datepub($data, "%Y");
    $out .= hal_api_extract_pagination($data, "nb");
    return($out);
}


// HAL - FONCTIONS D'EXTRACTION

function hal_api_extract_description($data) {
    $out = "";
    // Description
    $q = $data['description_s'];
    if (!empty($q)) $out .= ", ".$q;
    return($out);
}

function hal_api_extract_volumaison($data) {
    $out = "";
    // Volume
    $q = $data['volume_s'];
    if (!empty($q)) {
        $out .= $q;
        $res = preg_match( '/ol/', $q);
        if ($res!=1) $out = "volume ".$out;
        $out = ", ".$out;
    }
    // Numéro
    $q = $data['issue_s'][0];
    if (!empty($q)) {
        $res = preg_match( '/n/', $q);
        if ($res!=1) {
            $out .= ", n°".$q;
        } else {
            $out .= ", ".$q;
        }
    }
    return($out);
}

function hal_api_extract_datestart($data, $format="%B %Y") {
    $out = "";
    // Date début
    $q = $data['conferenceStartDate_s'];
    if (!empty($q)) {
        if (strlen($q)!=4) $q = utf8_encode(strftime($format, strtotime($q)));
        $out .= ", ".$q;
    }
    return($out);
}

function hal_api_extract_dateprod($data, $format="%B %Y") {
    $out = "";
    // Date de production
    $q = $data['producedDate_s'];
    if (!empty($q)) {
        if (strlen($q)!=4) $q = utf8_encode(strftime($format, strtotime($q)));
        $out .= ", ".$q;
    }
    return($out);
}

function hal_api_extract_datepub($data, $format="%B %Y") {
    $out = "";
    // Date publication
    $q = $data['publicationDate_s'];
    if (!empty($q)) {
        if (strlen($q)!=4) $q = utf8_encode(strftime($format, strtotime($q)));
        $out .= ", ".$q;
    }
    return($out);
}

function hal_api_extract_datesoutenance($data, $format="%e %B %Y") {
    $out = "";
    // Date soutenance
    $q = $data['defenseDate_s'];
    if (!empty($q)) {
        $date = $q;
        if (strlen($q)!=4) $q = utf8_encode(strftime($format, strtotime($q)));
        $out .= ", thèse soutenue le ".$q;
    }
    return($out);
}

function hal_api_extract_datesoutenancehdr($data, $format="%e %B %Y") {
    $out = "";
    // Date soutenance
    $q = $data['defenseDate_s'];
    if (!empty($q)) {
        $date = $q;
        if (strlen($q)!=4) $q = utf8_encode(strftime($format, strtotime($q)));
        $out .= ", HDR soutenue le ".$q;
    }
    return($out);
}


function hal_api_extract_pagination($data, $type="pos") {
    $out = "";
    // Pagination
    $q = $data['page_s'];
    if (!empty($q)) {
        $pages_ok = false;
        $res = preg_match( '/[0-9LXIV]/', $q);
        if ($res==1) $pages_ok = true;
        if ($pages_ok) {
            $out .= $q;
        }
        else {
            return("");
        }
        $res = preg_match( '/p\./', $q);
        if ($res!=1) {
            if ($type=="nb") $out = ", ".$out."&nbsp;p.";
            if ($type=="pos") $out = ", p.&nbsp;".$out;
        } else {
            $out = ", ".$out;
        }
    }
    return($out);
}


function hal_api_extract_title($data, $type="main") {
    $out = "";
    // Titre de l'objet
    $q = $data['title_s'];
    if (!empty($q)) {
        $out .= $q[0];
    }
    // Sous-titre de l'objet
    $q = $data['subTitle_s'];
    if (!empty($q)) {
        $out .= ". ".$q[0];
    }
    // Ajout url vers HAL
    $id_hal = $data['halId_s'];
    $out = '<a class="title-link" title="Notice sur HAL-SHS" href="https://hal.archives-ouvertes.fr/'.$id_hal.'">'.$out.'</a>';
    if ($out != "") {
        if ($type=="main") $out = ", <em>".$out."</em>";
        if ($type=="second") $out = ", «&nbsp;".$out."&nbsp;»";
    }
    return($out);
}

function hal_api_extract_collection($data) {
    $out = "";
    // Collection
    $q = $data['serie_s'][0];
    if (!empty($q)) $out .= ", «&nbsp;".$q."&nbsp;»";
    return($out);
}

function hal_api_extract_editeur($data) {
    $out = "";
    // Éditeur
    $q = $data['publisher_s'][0];
    if (!empty($q)) $out .= ", ".$q;
    return($out);
}

function hal_api_extract_auteurs($data) {
    $out = "";
    $dir = "";
    $quality = $data['authQuality_s'];
    $auteurs = $data['authFullName_s'];
    $struct = $data['structHasAuthId_fs'];
    if (!empty($quality)) {
        if (in_array("edt", $quality) or in_array("sad", $quality)) {
            for ($i = 0; $i < count($auteurs); ++$i) {
                if (($quality[$i] != "sad") and ($quality[$i] != "edt")) {
                    $auteurs[$i] = null;
                }
            }
            $auteurs = array_filter($auteurs);
            $dir = " (dir.)";
        }
    };
    if (!empty($struct)) {
        for ($i = 0; $i < count($auteurs); ++$i) {
            for ($j = 0; $j < count($struct); ++$j) {
                if (preg_match("/^136794_.*".$auteurs[$i]."/", $struct[$j])) {
                    $auteurs[$i] = "<strong>".$auteurs[$i]."</strong>";
                    break;
                }
            }
        }
    };

    $out = implode(", ", $auteurs) . $dir;
    return($out);
}

function hal_api_extract_liens($data, $recentes) {
    $out = "";
    // Lien HAL
    if ($recentes) {
        $id_hal = $data['halId_s'];
        $out .= '<a class="lien-notice" href="https://hal.archives-ouvertes.fr/'.$id_hal.'" title="Notice sur HAL" data-toggle="tooltip">HAL</a>';
    }
    // Présence document
    foreach ((array) $data['files_s'] as $file) {
        if (substr($file, 0, 4) == "http") {
            $out = $out . "<a href=\"".$file."\" title=\"Document en ligne sur HAL\">";
            $out .= "<i data-toggle='tooltip' title='Document en ligne sur HAL' class='bi-file-text' role='img' aria-label='Document en ligne sur HAL'></i>";
            $out .= "</a>";
        }
    }
    
    // URLs supplémentaires
    foreach ((array) $data['linkExtUrl_s'] as $ref) {
        if (substr($ref, 0, 4) == "http") {
            $out = $out . "<a href=\"".$ref."\" title=\"Ressource en ligne\">";
            $out .= "<i data-toggle='tooltip' title='Ressource en ligne' class='bi-link' role='img' aria-label='Ressource en ligne'></i>";
            $out .= "</a>";
        }
    }
    return($out);
}



// FILTRES HAL

function typdoc($data) {
    $t = $data['docType_s'];
    switch(strtolower($t)) {
    case 'art':
        $t = 'Articles';
        break;
    case 'comm':
        $t = 'Communications';
        break;
    case 'couv':
        $t = 'Chapitres d\'ouvrage';
        break;
    case 'douv':
        $t = 'Directions d\'ouvrage';
        break;
    case 'img':
        $t = 'Images';
        break;
    case 'lecture':
        $t = 'Cours';
        break;
    case 'map':
        $t = 'Cartes';
        break;
    case 'other':
        $t = 'Autres documents';
        break;
    case 'ouv':
        $t = 'Ouvrages';
        break;
    case 'patent':
        $t = 'Brevets';
        break;
    case 'poster':
        $t = 'Posters';
        break;
    case 'report':
        $t = 'Rapports';
        break;
    case 'software':
        $t = 'Logiciels';
        break;
    case 'son':
        $t = 'Sons';
        break;
    case 'these':
        $t = 'Thèses';
        break;
    case 'undefined':
        $t = 'Preprints, documents de travail';
        break;  
    case 'video':
        $t = 'Vidéos';
        break;
    case 'hdr':
        $t = 'HDR';
        break;
    case 'issue':
        $t = 'Numéros spéciaux de revues';
        break;  
    case 'proceedings':
        $t = 'Recueil de communications';
        break;     
    }
    return($t);
}

function genere_url_hal($tab) {
    if (preg_match('/^http/', $tab['url_hal'])==1) return($tab['url_hal']);
    $prenom = str_replace(" ", "+",$tab['prenom']);
    $nom = str_replace(" ", "+",$tab['nom']);
    $nom = $prenom."+".$nom;
    $idhal = $tab['idhal'];
    if (preg_match('/[a-z]/i', $idhal)) {
        // idHal
        $url = 'https://hal.archives-ouvertes.fr/search/index/?qa[authIdHal_s][]='.$idhal;
    }
    else {
        // Sinon prénom + nom + labo
        $url = 'https://hal.archives-ouvertes.fr/search/index/?qa[authFullName_t][]='.$nom.'&qa[structId_i][]=136794';
    }
    return($url);
}

// Récupère l'année de production d'une entrée de l'API
function annee_production($data) {
    $q = $data['publicationDate_s'];
    if (!empty($q)) {
        $q = substr($q, 0, 4);
    }
    return($q);
}

// Récupère le type de document
function get_typdoc($data) {
    $q = $data['docType_s'];
    return($q);
}


// FILTRES UTILITAIRES 

// Filtre qui renvoie l'URL d'une rubrique d'équipe en fonction du nom
function num_rubrique_equipe($nom) {
    $num = substr($nom, 0, 1);
    if ($num == 7) {
        $num = 132;
    } else {
        $num = 42 + $num;
    }
    return $num;
}


// Filtre qui trie un tableau associatif par ordre descendant
function tri_tableau($tab) {
    arsort($tab);
    return $tab;
}

// Filtre qui renvoie "de" ou "d'" en fonction de la première lettre du texte
function preposition_de($texte) {
    $first = strtolower(substr($texte, 0, 1));
    $voyelles = array("a","e","i","o","u","y","é","è","h");
    if (in_array($first, $voyelles)) {
        $result = " d'";
    } 
    else {
        $result = " de ";
    }
    return($result);
}

// Filtre qui renvoie le code font bootstrap-icons correspondant à l'icône du type de publi passé
// en paramètre
function icone_publi($type) {
    switch($type) {
        case "Audiovisuel":
            $result = "bi-film";
            break;
        case "Article":
            $result = "bi-file-text";
            break;
        case "Recension":
            $result = "bi-file-text";
            break;
        default:
            $result = "bi-book";
            break;
    }
    return($result);
}

