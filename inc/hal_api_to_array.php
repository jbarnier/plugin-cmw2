<?php

function inc_hal_api_to_array_dist($data) {

    $tab = json_decode($data, true);

    if ($tab === null) {
      return null;
    }

    return $tab['response']['docs'];

}