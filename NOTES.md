
# Nouvelle API HAL

Champs :

docType_s,conferenceTitle_s,scientificEditor_s,bookTitle_s,journalTitle_s,title_s,description_s,volume_s,issue_s,conferenceStartDate_s,publicationDate_s,producedDate_s,defenseDate_s,page_s,subTitle_s,halId_s,serie_s,publisher_s,authFullName_s,journalPublisher_s,city_s,files_s,linkExtUrl_s

Identifiant du labo : 136794


# Compilation CSS -----------------------------------------

Se placer dans le répertoire du plugin `cmw2` et lancer :

    npm run dist

Ou :

    npm run watch


# Bootstrap -----------------------------------------------

## Préparation

On télécharge les sources de bootstrap 4 qu'on met à la racine du plugin.

Dans le répertoire de bootstrap, on installe tous les modules nécessaires avec :

    npm install
    npm install jquery postcss-sass --save

On installe les modules nécessaires pour le plugin proprement dit avec :

    npm install
    npm install postcss-sass --save

## Compilation js

La compilation du javascript bootstrap se fait dans le répertoire de bootstrap :

    npm run js

On peut ensuite copier `dist/js/bootstrap.min.js` dans le dossier `js` du plugin.

## Compilation CSS

Modifier bootstrap-4.X.X dans `package.json` et `scss/cmw.scss`.

Compiler avec :

   npm run dist

Le résultat est dans `css/cmw.min.css`, il contient toutes les CSS (bootstrap + personnalisées CMW).

## En cas d'erreurs

En cas d'erreur lors de la compilation :

    rm -rf node_modules
    npm install

Si ça ne fonctionne toujours pas, récupérer les `dependencies` et `devDependencies` de `bootstrap/package.json` et les membres dans `cmw2/package.json`.

## Bootstrap-icons

Les icônes ne sont plus fournies par *Font awesome* mais par *Bootstrap icons*. Télécharger et décompresser le zip (ne pas installer via `npm`) à la racine. Attention, en cas de mise à jour il faut modifier les chemins dans `bootstrap-icons.scss` et dans les deux urls en tête de fichier rajouter `../` :
`../bootstrap-icons-1.8.0/`, etc.




# Crédits photos ---------------------------------------------------------

Photo Lyon de nuit (défaut) : https://unsplash.com/photos/a0kx5lkzcvs
Photo réverbères : https://unsplash.com/photos/C0fT7xLTkB4
Photo tour CL (sommaire) : https://unsplash.com/photos/oLWGI-Q76Yc
Photo foule (membres) : https://unsplash.com/photos/sN3xqfZZVxw
Photo papiers (logo rubrique par défaut) : https://unsplash.com/photos/_eoobUKLFAI
Photo salle et sièges (logo événement par défaut) : https://unsplash.com/photos/ewGMqs2tmJI
Photo papiers (logo publi par défaut) : https://unsplash.com/photos/V29UWcALNko
Photo motifs (bandeau laboratoire) : https://unsplash.com/photos/9f1gCaLkZBU
Photo bibliothèque (bandeau publications) : https://unsplash.com/photos/dERxI-Rna2E
Photo puzzle (bandeau recherche) : https://unsplash.com/photos/3y1zF4hIPCg
Photo sièges (bandeau événements) : https://unsplash.com/photos/HnUHOBuJ7s4
Photo jumelles (bandeau recherche) : https://unsplash.com/photos/YPCY9HEP6V8
Photo boussole (bandeau infos pratiques) : https://unsplash.com/photos/NZ2SlpcVw1Y
Photo Sorry (bandeau page 404) : https://unsplash.com/photos/C8jNJslQM3A

Photo construction (logo projets en cours) : https://unsplash.com/photos/nhCPOp4A2Xo
Photo laptop (logo doctorants) : https://unsplash.com/photos/vZJdYl5JVXY
Photo réunion (logo séminaires) : https://unsplash.com/photos/KdeqA3aTnBY
Photo girouette (logo accompagnement, ressources, formations) : https://unsplash.com/photos/zAWs-hKChYA
Photo structure (logo partenaires/réseaux) : https://unsplash.com/photos/YDj7S5iH1vY
Photo livres (logo thèses soutenues) : https://unsplash.com/photos/OQSCtabGkSY
Photo tasse (logo livret d'accueil) : https://unsplash.com/photos/Spdu7YT1O00

Photo motifs (logo séminaire transversal) : https://unsplash.com/photos/9f1gCaLkZBU
Photo feuille (logo séminaire doctoral) : https://unsplash.com/photos/hWUiawiCO_Y
Photo film (logo séminaire image animée) : https://unsplash.com/photos/8eQOBtgn9Qo
Photo sans abri (logo séminaire frontières du sans abrisme) : https://unsplash.com/photos/324p_yfAZhE
Photo code (logo séminaire e-Juris) : https://unsplash.com/photos/466ENaLuhLY
Photo médecins (logo Professions de santé et profession politique) : https://unsplash.com/photos/Pd4lRfKo16U


Photo saut de haies (logo parcours quantitatif) : https://unsplash.com/photos/kY2H30v6Bs4
Photo pinces (logo ateliers pratique du quanti) :
https://unsplash.com/photos/VdOO4_HFTWM
Photo foule (logo RDV doc) : https://unsplash.com/photos/gcDwzUGuUoI

Photo parking (logo laboratoire > présentation) : https://unsplash.com/photos/EI32DgwN450
Photo oies (logo laboratoire > équipes) : https://unsplash.com/photos/IrI889hknhc
Photo boussole (logo laboratoire > accompagnement) : https://unsplash.com/photos/_94HLr_QXo8


Logo rubrique équipe 1 : https://unsplash.com/photos/dRrWcufYhkg
Logo rubrique équipe 2 : https://unsplash.com/photos/AI6U6qiXquw
Logo rubrique équipe 3 : https://unsplash.com/photos/CXdeJI3bD2U
Logo rubrique équipe 4 : https://unsplash.com/photos/vWfKaO0k9pc
Logo rubrique équipe 5 : https://unsplash.com/photos/k_RYBedEvDw
Logo rubrique équipe 6 : https://unsplash.com/photos/n31JPLu8_Pw



# Arborescence du site ---------------------------------------

- Laboratoire
    - Présentation
    - Organigramme (ou direction)
    - Équipes
        - Travail, institutions, professions, organisations
        - Dynamiques sociales et politiques de la vie privée
        - Politiques de la connaissance
        - Cultures publiques
        - Dispositions, pouvoir, culture, socialisation
        - Modes, espace et processus de socialisation
    - Accompagnement de la recherche (pôle gestion, documentation, etc. sur une ou plusieurs pages)

- Recherche
    - Projets en cours
        - Projets terminés
    - Doctorant.e.s
        - Faire une thèse au CMW
    - Séminaires
    - Partenaires / réseaux

- Événements

- Publications

- Informations pratiques

- Membres


# Migration ---------------------------------------------------------


## Notes

Quand on met à jour le site de dev, ne pas oublier de synchroniser les fichiers de IMG avec rsync.

## Modif config du site

- permettre le HTML5 dans config avancée
- À rajouter dans `config/mes_options.php` :

```
// désactiver l'héritage des logos de rubriques
define('_LOGO_RUBRIQUE_DESACTIVER_HERITAGE', true); 
```

- Dans *Configuration*, *Fonctions avancées*, modifier la taille maximale des vignettes générées en 300.
- Enlever le descriptif rapide dans les articles
- Enlever le descriptif rapide dans les rubriques
- Activer l'utilisation des brèves


## Plugins

Désactiver le couteau suisse et Spip Bonux (?)


## Événements

Tous les événements et les actualités doivent être dans la rubrique Événements.

On rajoute des mots-clés pour tagguer les séminaires si on veut afficher leurs actus.

## Publications

On vire la rubrique publications du site qui ne sert actuellement à rien. On supprime toutes les sous-rubriques et articles qu'elles contiennent.
On y place comme articles toutes les annonces de publications.

Créer un groupe de mots-clés "types de publications" (id 5 normalement) avec les valeurs : ???

Déplacer les actualités actuelles dans la rubrique publications et attribuer les bons mots-clés.

## Annonces

Question : on crée des brèves dans la rubrique "Annonces" avec un mot-clé ? ou des articles dans des sous-rubriques de "Annonces" ?

## Annuaires

Supprimer la rubrique *Rechercher dans l'annuaire*.
Vider le texte de la rubrique *Annuaire*.
Renommer *Annuaire* en *Membres*.

Dans les champs auteur, mettre à jour ISH en MSH, et changer le nom de l'équipe Urbanités contemporaines en Cultures publiques.


##  Laboratoire et équipes

Renommer *Présentation* en *Laboratoire*
Créer une sous-rubrique *Équipes*
Déplacer les rubriques équipes dans cette sous-rubrique.
Supprimer mot-clé "rubrique d'équipe" ?

Créer une sous-rubrique "Accompagnement de la recherche". Y déplacer Intranet/Aide site Web et supprimer la rubrique Intranet.


## Mots-clés

Créer un groupe de mots-clés "types de publications" (id 5 normalement, sinon changer dans le squelette de la rubrique).

Créer un groupe de mots-clés "type d'événement) (id 6 normalement, sinon changer dans le squelette de la rubrique).

Créer un groupe de mots-clés "Type d'annonce", pour le brèves uniquements. Associer les mots-clés avec les identifiants dans le sommaire.

Dans les mots-clés de rubrique, on supprime "Rubrique générale" et "rubrique d'équipe", et on transforme "Rubrique générale avec onglets" en "Rubrique avec onglets".

On supprime le groupe de mots-clés "statut de l'opération de recherche".

On supprime le groupe de mots-clés "type d'actualité". (?)

## English

Créer une rubrique "21. Pages en anglais" et y mettre un article *English presentation*. Mettre à jour l'ID de l'article dans `header_end.html`.

## Code organigramme direction 

On utiliser le modèle `modele/direction.hmlt`

À insérer dans la page *Présentation* avec le code `<modele1>` ou `<modele|>`.

## Lettres de nouvelles

Archiver les anciennes lettres dans une sous-rubrique de la rubrique lettres, et leur appliquer l'ancien squelette `article-106_OLD.html` en le renommant.







# Notes ----------------------------------------------------------------

## Arborescence 

Arborescence prévue lors de la première réunion :

![Arbo mi-2017](./Arborescence_site_CMW.png)

### Remarques de Magali

#### Rubrique laboratoire

Ne faire qu'un seul niveau de menu pour la présentation du labo. Soit une unique
page, soit plusieurs sous-articles

Pour l'organigramme, idée de mettre des photos des directeurs, DA, etc. (voir
modèle de page fait par Magali).

Pour les pages équipes, soit l'équipe a une unique page de présentation, sinon
partir sur un système d'onglets.

Du coup on aurait Labo avec sous-pages : présentation (dont historique),
direction ou organigramme, équipes, ressources.

#### Rubrique ressources

L'appellation n'est pas bonne, du point de vue d'un visiteur extérieur ça
apparaît comme des ressources *pour lui*. À faire passer dans la rubrique
*Laboratoire*.

À voir comment on structure, on peut peut-être tout regrouper en une seule page.

#### Rubrique membres

On fait une seule page, avec la possibilité de filtrer par statut, par équipe ou
par site géographique, et ensuite un affichage par onglet (un onglet par site ou
un onglet par statut, etc.), comme sur le site de l'Ihrim. Affichage sous forme
de grille avec des photos. Pour faciliter la navigation on ajoute un champ qui
permet de filtrer dynamiquement sur le nom et/ou le prénom.

Mention chercheur associé à rajouter sur les fiches.

Question de différencier les post-doctorants des doctorants (à rajouter aux chercheurs).

#### Archives d'actualités

On fait des pages d'archives d'événements du labo. Accessible via les boutons "tous les événements" de la page d'accueil. Sans doute aussi depuis une rubrique "événements" dans le menu (à voir).

Dans ces archives, privilégier une présentation sous forme de grille comme sur le site de l'Ihrim :

http://ihrim.ens-lyon.fr/manifestations/


#### Page d'accueil

Renommer "Actualité" du carrousel en "À la une", en "zoom sur" ou en rien du
tout.

Le bloc "Annonces"

## Gestion des actualités

### Type d'actualités

- **Vie du labo** : pas d'archivage. Pas forcément affiché sur le site.
- **Vie scientifique** : toujours associé à un événement. Affichage sur la page d'accueil dans le bloc événements. Archivé dans l'agenda.
- **Publications** : géré à part
- **Annonces : médias et distinction** : souvent vide, à afficher dans les colonnes actualités. Archiver dans une rubrique à part.
- **Annonces : appels à contribution / candidatures** : à afficher dans les colonnes actualités. Pas d'archivage.
- **Annonces : financement de la recherche** : à afficher dans les colonnes actualités. Pas d'archivage.
- **Annonces : accompagnement / formation** : à afficher dans les colonnes actualités. Pas d'archivage.

### Génération de la lettre

Mode de sélection des actus : demander la date de la lettre. Pour les
événements, sélectionner tous ceux qui sont à date + 1 mois. Pour les autres,
sélectionner tous ceux dont la date de publication est à date - 1 semaine.

Permettre de sélectionner manuellement les titres correspondant dans un deuxième temps.

Pour la sortie deux possibilités : génèrer directement le HTML pour envoi par
mail (avec nettoyage préalable) ou générer un article Spip qui peut être édité
ensuite.

Dans le premier cas, question de l'archivage et d'une URL propre.

À voir aussi les plugins `Newsletter` et `Ma lettre`.

Prévoir la possibilité d'avoir des infos sur le site mais pas dans la lettre, et
inversement.

## Rubrique publications 

Bien mentionner qu'il s'agit d'une sélection, et pas de l'exhaustif.

On part sur la mise en valeur des trois dernières publis comme sur la page d'accueil du site, puis affichage avec onglets comme pour la page Membres. On propose d'afficher par date (pas d'onglet, chronologique), par type de publis ou par équipe (onglets, chronologique avec pagination).

Pour la saisie, une publi = un article dans la rubrique Publications, avec titre du doc dans Titre, auteurs dans sous-titre, Infos biblio dans Descriptif rapide, deux mots clés pour type de publi et équipe, et image de couverture en logo.

Élisa : Création d'une collection HAL CMW dans HAL-SHS. Permet d'être alertée à
chaque nouvelle publication.


