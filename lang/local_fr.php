<?php
$GLOBALS[$GLOBALS['idx_lang']] = array(
  'pass_oubli_mot' => 'Rappel d\'identifiants',
  'pass_mail_passcookie' => '
  (ceci est un message automatique)

  Bonjour,

  Pour pouvoir vous connecter au site
  @nom_site_spip@, veuillez vous rendre à l\'adresse suivante :
  
  @sendcookie@
  
  Vous pourrez alors choisir ou modifier vos identifiants.
  '
);
?>
  
