#!/bin/sh

rsync -Pavz * cmw:/homez.760/centrema/www/plugins/cmw2/ --exclude .git --exclude bootstrap-4.6.1 --exclude node_modules/ --exclude sync.sh
exit 0
